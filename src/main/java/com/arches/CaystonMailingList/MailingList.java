package com.arches.CaystonMailingList;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import org.apache.commons.lang.WordUtils;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import com.arches.model.ShortUrl;
import com.arches.utilities.Bitlytest;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.rosaloves.bitlyj.ShortenedUrl;

import net.swisstech.bitly.BitlyClient;
import net.swisstech.bitly.model.Response;
import net.swisstech.bitly.model.v3.ShortenResponse;



public class MailingList {

	public List<String> getMailingList()
	{
		List<String> lines=new ArrayList<String>();
		Configuration cfg=null;
			SessionFactory sessionFactory=null;
			Session session=null;
			String line="".trim();
			
			try
			{
				
				System.out.println("getting users..");
				cfg=new Configuration();
				cfg.configure(CaystonMailingListMain.hibernatePath);
				sessionFactory=cfg.buildSessionFactory();
				session=sessionFactory.openSession();
		
				//gete all users from LFT
				String hql1="select UD.uuid,UD.capPatientId, LOWER(UD.firstName),LOWER(UD.lastName),LOWER(UD.address1),LOWER(UD.address2),LOWER(UD.city),UPPER(UD.state),UD.zip,UD.contactNumber from UserDet UD where  (UD.zip is not null and UD.zip!='')  and (UD.address1 is not null and UD.address1!='')  and (UD.email is null or UD.email='') and (:currentTS-UD.createdOn<=24*60*60*1000)";
				Query query1= session.createQuery(hql1);
				query1.setParameter("currentTS",new BigDecimal(System.currentTimeMillis()));
				List results=query1.list();
			
				System.out.println("Starting iterator to iterator over mailing result");
				Bitlytest bitlytest=new Bitlytest(CaystonMailingListMain.bitlyUsername,CaystonMailingListMain.bitlyPassword);
				int sleepTimer=0;
				for (Iterator it = results.iterator(); it.hasNext();)
				{ //for each record
					sleepTimer++;
					System.out.println("entered iterator...");
					
					Object[] row = (Object[]) it.next();		
					line="";
					
					for (int i = 0; i < row.length; i++)		
					{
						
						System.out.println("getting data from row"+row[i]);
						if(row[i]==null)
						{
							row[i]="".trim();
						}
						//for content within each record fetched form a row
							
							line=line+"\"";
					
							if(i==row.length-3)
								{
								//for state
								line=line+((String)row[i]);
								}
							else if((i==4) || (i==5))
							{
								
								//handle comma case for address1 and address2.replace comma with space to ensure correctness in csv file
								if(((String)row[i]).contains(","))
								{
									
									row[i]=((String)row[i]).replace(","," ");
									
								}
								line=line+WordUtils.capitalize((String)row[i]);
							}
							else
							{
								//every field other than state 
								line=line+WordUtils.capitalize((String)row[i]);
							}
							
							
							if(i!=row.length-1)
								{
								line=line+"\",";
								}
							else if(i==row.length-1)
							{
								
								//if row.length-1 is reached. just add short url at end
								line=line+"\",";
								String shortURL=null;
								try
								{
							shortURL=bitlytest.getShortUrl(CaystonMailingListMain.mailingListBaseURL+((String)row[0]).trim()+CaystonMailingListMain.mailingListBaseURLPart1+CaystonMailingListMain.mailingListBaseURLPart2);
							/*		BitlyClient client = new BitlyClient("b4ced36e3d674939b1fe3f8ba0d03a10774f335b");
									JsonParser parser = new JsonParser();
									
									
											Response<ShortenResponse> respShort = client.shorten().setLongUrl(CaystonMailingListMain.mailingListBaseURL+((String)row[0]).trim()+CaystonMailingListMain.mailingListBaseURLPart1+CaystonMailingListMain.mailingListBaseURLPart2).call();
											JsonObject obj = parser.parse(respShort.toString()).getAsJsonObject();
											shortURL=obj.get("status_code").getAsString();
											*/
								}
								catch(Exception  e)
								{
									e.printStackTrace();
									System.out.println("bitly exception");
									break;
								}
								
								line=line+shortURL;
								line=line+"\"";
							}
						}
						System.out.println(line);
					
					lines.add(line);
						if(sleepTimer%90==0)
						{
							System.out.println("60 seconds pause..");
							Thread.sleep(60*1000);
						}
					
				}
			
			}
			catch(Exception e)
			{
				e.printStackTrace();
				CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,e.getMessage(),e);
		
			}
			finally
			{
				if(session!=null)
				{
					System.out.println("close hibernate session!!!");
					session.flush();
					session.clear();
					session.close();
				}
				System.out.println("returning lines of size.."+lines.size());
				return lines;
			}
		
	}
	
}
