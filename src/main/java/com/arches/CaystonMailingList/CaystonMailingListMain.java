package com.arches.CaystonMailingList;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import java.util.TimeZone;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import org.apache.commons.io.FileUtils;
import org.supercsv.io.CsvListWriter;
import org.supercsv.io.ICsvListWriter;
import org.supercsv.prefs.CsvPreference;

import com.arches.pgp.PGPTool;
import com.arches.utilities.DateTime;

public class CaystonMailingListMain {

	public static Logger caystonMailingListLogger;
	public static String hibernatePath;
	public static String mailingListErrorLogsLocation; 
	public static String mailingListTXTPath;
	public static String mailingListPGPPath;
	public static String passphrase;
	public static String key;
	public static String mailingListBaseURL;
	public static String mailingListBaseURLPart1;
	public static String mailingListBaseURLPart2;
	public static String bitlyUsername;
	public static String bitlyPassword;
	public static String configFileLocation;
	public static String mailingListCSVFileLocation;
	public static String encryptedMailingListCSVFileLocation;
	static FileHandler fileHandler;
	
	public static void initializeApp()
	{
		configFileLocation="config";
		ResourceBundle configBundle=ResourceBundle.getBundle(configFileLocation);
		hibernatePath=configBundle.getString("hibernatePath").trim();
		mailingListErrorLogsLocation=configBundle.getString("mailingListErrorLogsLocation").trim();
		//mailingListTXTPath=configBundle.getString("mailingListTXTPath").trim();
		//mailingListPGPPath=configBundle.getString("mailingListPGPPath").trim();
		passphrase=configBundle.getString("PASSPHRASE").trim();
		//key=configBundle.getString("KEY").trim();
		mailingListBaseURL=configBundle.getString("mailingListBaseURL").trim();
		mailingListBaseURLPart1=configBundle.getString("mailingListBaseURLPart1").trim();
		mailingListBaseURLPart2=configBundle.getString("mailingListBaseURLPart2").trim();
		bitlyUsername=configBundle.getString("bitlyUsername").trim();
		bitlyPassword=configBundle.getString("bitlyPassword").trim();
		mailingListCSVFileLocation=configBundle.getString("mailingListCSVFileLocation").trim();
		encryptedMailingListCSVFileLocation=configBundle.getString("encryptedMailingListCSVFileLocation").trim();
			
		try {
	  		fileHandler=new FileHandler(mailingListErrorLogsLocation+"MailingListErrorLogs_"+DateTime.getDateTime()+".txt",true);
	  		caystonMailingListLogger=Logger.getLogger("caystonLFTLogger");
	  		caystonMailingListLogger.addHandler(fileHandler);
	  		SimpleFormatter simpleFormatter=new SimpleFormatter();
	  		fileHandler.setFormatter(simpleFormatter);
	  	   caystonMailingListLogger.log(Level.INFO,"Logging for UTC date:"+DateTime.getDateTime(),"Logging for UTC date:"+DateTime.getDateTime());
	  	}
	  	catch(Exception e)
	  	{
	  		System.out.println("Exception in configuring logger."+e);
	  		e.printStackTrace();
	  	}

	}
	public static void main(String[] args) 
	{
		// TODO Auto-generated method stub
		initializeApp();
		MailingList users=new MailingList();
		List<String> lines=users.getMailingList();
		long count=0;
		if(lines!=null)
		{
			count=lines.size();
		}
		//boolean success=prepareFile(lines);
		//do not generate txt file. just the csv file
		
		
		try
		{
			
		boolean csvSuccess=writeCSV(lines);
			if(csvSuccess==false)
			{
				caystonMailingListLogger.log(Level.SEVERE,"Error in writing records to csv file. Method returned false","[Main]writeCSV file returned false");
			}
		}
		catch(Exception e)
		{
			caystonMailingListLogger.log(Level.SEVERE,"Error in writing records to csv file. Method caused exception","[Main]writeCSV file returned false");
		}
			finally
			{
				for(Handler h:caystonMailingListLogger.getHandlers())
				{ if(h!=null)
				    h.close();   //must call h.close or a .LCK file will remain.
				}
			}
			
		
		System.out.println(" processing complete..");
		System.exit(0);
	
		
	}

	public static boolean prepareFile(List<String> lines)
	{
		boolean success=false;
		if(lines==null)
		{
			lines=new ArrayList<String>();
		}
		long count=lines.size();
	//	lines.add(0,Calendar.getInstance(TimeZone.getTimeZone("EST")).getTime().toString());
		
		File file=new File(mailingListTXTPath+"ARCHES_CAYSTON_MAILING_LIST_"+DateTime.getDateTime()+"_"+count+".txt");
		//true for append mode
		try {
			FileUtils.writeLines(file,lines,true);
			success=true;
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success=false;
			caystonMailingListLogger.log(Level.SEVERE,e.getMessage(),e);
		}
		finally
		{
			return success;
		}

	}
	
	public static boolean writeCSV(List<String> list) {

		boolean success=true;
	    //create a File class object and give the file the name employees.csv
		String inputFilePath=mailingListCSVFileLocation+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+list.size()+".csv";
		String outputFilePath=encryptedMailingListCSVFileLocation+"ARCHES_GILEAD_CAYSTON_CRM_ENROLLMENT_"+DateTime.getDateTime()+"_"+list.size()+".csv.pgp";
	if(list==null)
	{
		list=new ArrayList<String>();
	}
		
		Iterator it=list.iterator();
		String record[]=null;
		String header[]={"UUID","CAP ID","First Name","Last Name","Address1","Address2","city","State","Zip","Contact_Number","Short URL"};
				ICsvListWriter csvWriter = null;
        try {
            csvWriter = new CsvListWriter(new FileWriter(inputFilePath), CsvPreference.STANDARD_PREFERENCE);
            csvWriter.writeHeader(header);
            for (int i = 0; i < list.size(); i++)
            {
            	record=list.get(i).replace("\"","".trim()).split(",");
               csvWriter.write(record);
              
            }
        	ResourceBundle resource = ResourceBundle.getBundle(CaystonMailingListMain.configFileLocation);
    		PGPTool pgpTool=new PGPTool();
    		try {
    			
    			pgpTool.testEncrypt(inputFilePath,outputFilePath,resource.getString("PASSPHRASE"),resource.getString("MAILKEY"));
    		} catch (Exception e) {
    			// TODO Auto-generated catch block
    			
    			e.printStackTrace();
    			   CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,e.getMessage(),e);
    			   success=false;
    			
    		}
        } 
        catch (IOException e) 
        {
            e.printStackTrace(); // TODO handle exception properly
            CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,e.getMessage(),e);
            success=false;
        }
        finally {
            try
            {
                csvWriter.close();
            }
            catch (IOException e) {
            	e.printStackTrace();
            	  CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,e.getMessage(),e);
            	success=false;
            }
            return success;
        }
        
        } //end writeCSV()

	

	
}
