package com.arches.pgp;

import java.util.ResourceBundle;
import java.util.logging.Level;
import com.arches.CaystonMailingList.CaystonMailingListMain;

public class PGPTool {

	public PGPTool(){
System.out.println("PGPTool started..");
	}
	public  boolean  testDecrypt(String dInput,String dOutput,String passphrase,String key) throws Exception {
		PGPFileProcessor p = new PGPFileProcessor();
		p.setInputFileName(dInput);
		p.setOutputFileName(dOutput);
		p.setPassphrase(passphrase);
		p.setSecretKeyFileName(key);
		boolean result= p.decrypt();
		if(result==false)
		{
			CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,"Decryption failure: false returned","Decryption failure");
		}
		return result;
	}

	public boolean testEncrypt(String eInput, String eOutput, String passphrase,String key) throws Exception {
		PGPFileProcessor p = new PGPFileProcessor();
		p.setInputFileName(eInput);
		p.setOutputFileName(eOutput);
		//p.setPassphrase(passphrase);
		p.setPublicKeyFileName(key);
		boolean result=p.encrypt();
		if(result==false)
		{
			CaystonMailingListMain.caystonMailingListLogger.log(Level.SEVERE,"Encryption failure: false returned","Encryption failure");
		}
		return result;
	}
	

}
