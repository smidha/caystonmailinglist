package com.arches.utilities;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

public class Bitlytest
{
	/** Change this if you want to use j.mp instead. */
	public static BitlyService service = BitlyService.BITLY;

	private final String bitlyAuth;

	private org.apache.http.client.HttpClient httpclient = null;

	/** Last response kept in case user gets exception and wants to see the response from bit.ly. */
	private BitlyReply lastResponse = null;
	
public Bitlytest(String login, String apiKey) {
		bitlyAuth = "&format=json&login=" + login + "&apiKey=" + apiKey;
		httpclient = new DefaultHttpClient();
	}

public String getShortUrl(String urlToShorten) throws Exception {
		BitlyReply reply = null;
		String httpResponse = null;
		httpResponse = getBitlyHttpResponseText(urlToShorten);
		reply = new BitlyReply(urlToShorten, httpResponse);
		lastResponse = reply;
		return reply.getShortUrl();
	}

	BitlyReply getBitlyReply(String urlToShorten) throws JSONException, IOException {
		String httpResponse = getBitlyHttpResponseText(urlToShorten);
		return new BitlyReply(urlToShorten, httpResponse);
	}
	
	private String getBitlyHttpResponseText(String urlToShorten) throws IOException {
		String uri = getBitlyUrl() + URLEncoder.encode(urlToShorten) + bitlyAuth;
		HttpGet httpGet = new HttpGet(uri);
		HttpResponse response = httpclient.execute(httpGet);
		String json = getText(response);
		return json;
	}
	
	private String getText(HttpResponse response) throws IOException {
		InputStream is = response.getEntity().getContent();
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		while ((line = reader.readLine()) != null) {
			sb.append(line);
		}
		return sb.toString();
	}
	
	public BitlyReply getLastResponseFromBitLy() {
		return lastResponse;
	}

	private String getBitlyUrl() {
		//return "http://api." + Bitlytest.service + "/shorten?version=2.0.1&longUrl=";
                return "https://api-ssl." + Bitlytest.service + "/shorten?version=2.0.1&longUrl="; //https://api-ssl.bitly.com/
	}

	/** Represents a response from bit.ly. Mimics the structure of the JSON data. */
	
	/** Result -object which contains the shortUrl. */
	public enum BitlyService {
		BITLY {
			@Override
			public String toString() {
				return "bit.ly";
			}
		},
		JMP {
			@Override
			public String toString() {
				return "j.mp";
			}
		}
	}
	/*
	 * Sample response from bit.ly
	 * 
	 * <{"errorCode": 0,     
	 * "errorMessage": "",     
	 * "results": { "http://johnsenf.blogspot.com/2010/01/android-app-published-1-week-ago.html": 
	 * 			  { "hash": "7oa3Mf",  
	 * 				"shortCNAMEUrl": "http://bit.ly/dBdV3d", 
	 * 				"shortKeywordUrl": "", 
	 * 				"shortUrl": "http://bit.ly/dBdV3d",
	 * 				"userHash": "dBdV3d"
	 * 			  }},    
	 * "statusCode": "OK"}>
	 */
        
         /*public static void main(String args[]){
             try{
             Bitlytest objTest = new Bitlytest("yverma","R_0767a8d49cec49e69b27d999cac55188");
             String shortUrl = objTest.getShortUrl("https://mycaystonpartner.com");//("http://52.9.237.127/portal/#/home?token=54d559dd-4002-4173-933c-f8cb0075f741$1468771431541$&bm=pullArticleContent#pullArticleContent");
             System.out.println("short url is:"+shortUrl);
             }catch(Exception ex){
                 System.out.println("Exception occured "+ ex.getMessage());
             }
         }*/
        
}