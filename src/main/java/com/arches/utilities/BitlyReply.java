package com.arches.utilities;

import org.json.JSONException;
import org.json.JSONObject;


public class BitlyReply
{
	public String longUrl = "";
	public Integer errorCode;
	public String errorMessage;
	public String statusCode;
	public BitlyResult result;
	
	public String getShortUrl()
	{
		return result.shortUrl;
	}
	public BitlyReply(String longUrl, String jsonText) throws JSONException 
	{
		this.longUrl = longUrl;
		JSONObject bitlyMessage = new JSONObject(jsonText);
		this.errorCode = bitlyMessage.getInt("errorCode");
		this.errorMessage = bitlyMessage.getString("errorMessage");
		this.statusCode = bitlyMessage.getString("statusCode");
		JSONObject results = bitlyMessage.getJSONObject("results");
		JSONObject urlResult = results.getJSONObject(longUrl);
		result = new BitlyResult(urlResult.getString("hash"), urlResult.getString("shortCNAMEUrl"),urlResult.getString("shortKeywordUrl"),urlResult.getString("shortUrl"), urlResult.getString("userHash"));
	}	
}
