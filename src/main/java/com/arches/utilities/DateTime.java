package com.arches.utilities;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;


public class DateTime {
    
    
    public static String getDateTime()
    { 
        java.util.Date dt = new java.util.Date();
        java.text.SimpleDateFormat sdf =new java.text.SimpleDateFormat("yyyyMMdd");
        sdf.setTimeZone(TimeZone.getTimeZone("UTC"));
        return sdf.format(dt);
               
   } 
  public static Date StringToDate(String s) throws ParseException
  {
      SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
      format.setTimeZone(TimeZone.getTimeZone("UTC"));
       Date date = format.parse(s);
       return date;
  }

    public static long daysdiff(String d1,String d2)
    {
        DateFormat format = new SimpleDateFormat("yyyyMMdd");
        long diff=0;
            try {
                Date date1 = format.parse(d1);
                Date date2 = format.parse(d2);
                diff=TimeUnit.DAYS.convert(date1.getTime()-date2.getTime(),TimeUnit.MILLISECONDS);
 
               } catch (ParseException ex) {
                    Logger.getLogger(DateTime.class.getName()).log(Level.SEVERE, null, ex);
                    }   
         return diff;  
    }
    public static long hoursdiff(String d1,String d2)
    {
     DateFormat format = new SimpleDateFormat("yyyyMMdd");
        long diff=0;
            try {
                Date date1 = format.parse(d1);
                Date date2 = format.parse(d2);
                diff=TimeUnit.HOURS.convert(date1.getTime()-date2.getTime(),TimeUnit.MILLISECONDS);
                }
            catch (ParseException ex) {
                    Logger.getLogger(DateTime.class.getName()).log(Level.SEVERE, null, ex);
                    }   
         return diff;  
    }
}