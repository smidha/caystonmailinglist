package com.arches.model;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "lung_func_tracker", catalog = "cayston_prod")

public class LungFuncTracker implements Serializable{
	
	private BigDecimal id;
	private String user_uid;
	private Integer value;
	private String dateExamOn;
	private Byte isActive;
	private Byte isDeleted;
	private Integer status;
	private BigDecimal createdOn;
	private String createdBy;
	private BigDecimal modifiedOn;
	private String modifiedBy;
	private String uuid;
	

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false, scale = 0)
	
	public BigDecimal getId() {
		return id;
	}
	public void setId(BigDecimal id) {
		this.id = id;
	}
	@Column(name = "user_uid")
	public String getUser_uid() {
		return user_uid;
	}
	public void setUser_uid(String user_uid) {
		this.user_uid = user_uid;
	}
	@Column(name = "value")
	public Integer getValue() {
		return value;
	}
	public void setValue(Integer value) {
		this.value = value;
	}
	@Column(name = "dateexamon")
	public String getDateExamOn() {
		return dateExamOn;
	}
	public void setDateExamOn(String dateExamOn) {
		this.dateExamOn = dateExamOn;
	}
	@Column(name = "is_active")
	public Byte getIsActive() {
		return isActive;
	}
	public void setIsActive(Byte isActive) {
		this.isActive = isActive;
	}
	@Column(name = "is_deleted")
	public Byte getIsDeleted() {
		return isDeleted;
	}
	public void setIsDeleted(Byte isDeleted) {
		this.isDeleted = isDeleted;
	}
	@Column(name = "status")
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	@Column(name = "created_on")
	public BigDecimal getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(BigDecimal createdOn) {
		this.createdOn = createdOn;
	}
	@Column(name = "created_by")
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	@Column(name = "modified_on")
	public BigDecimal getModifiedOn() {
		return modifiedOn;
	}
	public void setModifiedOn(BigDecimal modifiedOn) {
		this.modifiedOn = modifiedOn;
	}
	@Column(name = "modified_by")
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	@Column(name = "uuid")
	public String getUuid() {
		return uuid;
	}
	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
