package com.arches.model;

import java.io.Serializable;

public class ShortUrl implements Serializable{
String status_code;
String status_text;
String data;
public String getStatus_code() {
	return status_code;
}
public void setStatus_code(String status_code) {
	this.status_code = status_code;
}
public String getStatus_text() {
	return status_text;
}
public void setStatus_text(String status_text) {
	this.status_text = status_text;
}
public String getData() {
	return data;
}
public void setData(String data) {
	this.data = data;
}

}
